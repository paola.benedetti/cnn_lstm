import gdal
import numpy as np
import sys

#Normalization to avoid saturation (constant accuracy values) while performing training
def normalizeB(mnew):
    max_ = np.amax(mnew)
    min_first = np.amin(mnew)
    print "==========================="
    print "shape band matrix ",mnew.shape
    print "max: ",max_
    print "first_min: ",min_first

    if max_ == min_first:
        return mnew * 0
    else:
        min_second = np.amin(np.array(mnew)[mnew != min_first])
        print "min_second: ",min_second
        print "==============================="
        #return ((mnew.astype('float32') - min_) / (max_ - min_) * 512) - 256


        return ( (mnew.astype('float32') - min_second )/(max_ - min_second) )

#normalize each band by the max and min values found over all 22 timestamps
def normalizeBT(ds_img,max_bands,min_bands,time_stamps):
    ds_n=[]
    for i in range(len(ds_img)):
        print "EXTRACTING index %d in ds"%i
        print "\textraction dimension: ",np.array(ds_img[i]).shape
        t_matrix = ds_img[i].astype('float32')
        t_matrix = ( t_matrix - min_bands[ i % time_stamps ] ) / (max_bands[ i % time_stamps] - min_bands[i%time_stamps])
        #t_matrix = (t_matrix * 512) - 256
        #print "max %f - min %f " % (np.amax(t_matrix), np.amin(t_matrix))
        ds_n.append(t_matrix)
    return ds_n

def writeHRsetN2(fileName, coordinates, outFileName):

    time_stamps = 22
    const = 30
    hr_list = []
    dsnew = gdal.Open(fileName)
    n_band=0

    #For each band get max and min values over all 22 timestsmps
    min_bands=[sys.float_info.min] * (time_stamps)
    max_bands=[sys.float_info.max] * (time_stamps)

    print "BANDE:%d"%dsnew.RasterCount

    for band in range(1,dsnew.RasterCount+1):
        matrix = dsnew.GetRasterBand(band).ReadAsArray()

        print "\n Band %d" % band
        print "matrix dim: ",matrix.shape
        max_=np.amax(matrix)
        print "Max_:",max_
        min_first=np.amin(matrix)
        print "Min_first:",min_first


        #use second minimum number to avoid NO_DATA value
        if max_ == min_first:
            min_second = np.amin( matrix )
        else:
            #print np.bincount(matrix)
            min_second = np.amin(np.array(matrix)[matrix != min_first])
        print "min_second: ",min_second
        print "=========================="

        min_second = min_first

        if max_bands[ (band-1) % time_stamps ] < max_:
            max_bands[ (band-1) % time_stamps ] = max_

        if min_bands[ (band-1) % time_stamps ] > min_second:
            min_bands[ (band-1) % time_stamps ] = min_second

        hr_list.append(matrix)
        n_band=band

    matrix = normalizeBT(hr_list,max_bands,min_bands,time_stamps)
    hr_list = []
    list_aux = []
    matrix = np.array(matrix)

    print "matrix: ", matrix.shape
    #START TO ITERATE OVER COORDINATES
    for i in range(1,n_band+1):
        print "iter:%d"%i
        index = 0
        for pix, piy in coordinates:
            #IF THE BAND IS THE FIRST ONE, ADD A LIST TO THAT POSITION
            if band == 1:
                hr_list.append(list_aux)

            idx_start = pix * const
            idx_end = (pix+1) * const
            idy_start = piy * const
            idy_end = (piy+1) * const

            #print "\t\t%d, %d correspond to (%d,%d) and (%d,%d) " % (pix,piy, idx_start, idx_end, idy_start, idy_end)

            print "%d:%d, %d:%d"%( idx_start, idx_end, idy_start, idy_end )
            print "copying: ",matrix[idx_start:idx_end , idy_start:idy_end].shape

            hr_list.insert(index, matrix[idx_start:idx_end , idy_start:idy_end]   )

            print "hr_list: ",len(hr_list)
            index+=1

    dsnew = None
    np.save(outFileName, np.array(hr_list))



def writeHRsetN1(fileName, coordinates, outFileName):
    const = 30
    dsnew = gdal.Open(fileName)
    for band in range(1,dsnew.RasterCount+1):

        print "We are analyzing band %d" % band

        matrix = dsnew.GetRasterBand(band).ReadAsArray()
        #Normalize the band between 0-1
        matrix = normalizeB(matrix)

        #START TO ITERATE OVER COORDINATES
        index = 0
        hr_list = []
        for pix, piy in coordinates:
            #IF THE BAND IS THE FIRST ONE, ADD A LIST TO THAT POSITION
			if band == 1:
				hr_list.append([])

			idx_start = pix * const
			idx_end = (pix+1) * const
			idy_start = piy * const
			idy_end = (piy+1) * const
			print "\t\t%d, %d correspond to (%d,%d) and (%d,%d) " % (pix,piy, idx_start, idx_end, idy_start, idy_end)

			hr_list[index].append( matrix[idx_start:idx_end , idy_start:idy_end]   )

			index+=1

        np.save(g_path+"/orig/HR_band%d.npy"%band, np.array(hr_list))
    #hr_list = np.reshape(hr_list,[len(coordinates),const,const])
	#np.save(outFileName, np.array(hr_list))


#N1 or N2 depends on which normalization we want to apply to data
TAG = sys.argv[1]
g_path = "../../dataset/%s/dati_m/"%TAG
fileName = '../../DATA/IMG_DATA/IMG_PHR1A_PANSHARP_CUT_201410191154274_ORT.TIF'

outFileName =g_path+"orig/newHR_img.npy"

var_path_label=g_path+"orig/ds_label.npy"

ds_labels = np.load(var_path_label)

print "load label ds .npy"
coordinates = ds_labels[:,0:2]

if TAG == 'N1':
    writeHRsetN1(fileName, coordinates, outFileName)
if TAG == 'N2':
    writeHRsetN2(fileName, coordinates, outFileName)
