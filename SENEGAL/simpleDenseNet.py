import numpy as np
import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data

def ConvBnPool(x):
    print "\n=========ConvBnPool==========="
    print "input: ",x.get_shape()

    conv7 = tf.layers.conv2d(x,
                            filters = 64,
                            kernel_size = [ 7, 7 ],
                            strides = ( 2, 2 ),
                            padding = "same",
                            activation = tf.nn.relu)

    print"conv7: ",conv7.get_shape()

    bn = tf.layers.batch_normalization(conv7)

    print"bn: ",bn.get_shape()

    max_pool = tf.layers.max_pooling2d( bn,
                                       pool_size = [3, 3],
                                       strides=2)

    print"max_pool: ",max_pool.get_shape()
    print"================================="
    print"================================="


    return max_pool

def DenseBlock(x,drop_rate,itr):
    print"\n=======DenseBlock_%d==========="%itr

    conv1 = tf.layers.batch_normalization(x)
    conv1 = tf.nn.relu(conv1)

    conv1 = tf.layers.conv2d( conv1,
                              filters=32,
                              kernel_size=[1, 1],
                              padding="valid")



    conv3 = tf.layers.batch_normalization(conv1)
    conv3 = tf.nn.relu(conv3)

    conv3 = tf.layers.conv2d( conv3,
                              filters=64,
                              kernel_size=[3, 3],
                              padding="same")

    drop_out = tf.layers.dropout( conv3,
                                 rate=drop_rate,
                                 noise_shape=None,
                                 seed=None,
                                 training=False,
                                 name=None)
    print"input: ",x.get_shape()
    print "conv1x1: ",conv1.get_shape()
    print "conv3x3: ",conv3.get_shape()
    print"drop_out: ",drop_out.get_shape()
    print"================================="
    print"================================="
    return drop_out

def ClassificationLayer(x):
    print"\n=======ClassificationLayer==========="
    print"input: ",x.get_shape()
    conv = tf.layers.conv2d( x,
                            filters = 64,
                            kernel_size=[1,1],
                            activation = tf.nn.relu )

    print"conv: ",conv.get_shape()


    #pool = tf.layers.avg_pooling2d( conv,
    #                               pool_size=[2, 2],
    #                               strides=2)
    pool_flat = tf.reduce_mean(conv, [1,2])

    print"pool: ",pool_flat.get_shape()


    #pool_flat = tf.layers.flatten(pool)
    #pool_flat = tf.reshape(pool, [-1, 7 * 7 * 64])

    #print("pool_flat: ",pool_flat.get_shape())


    features = tf.layers.dense( pool_flat,
                            units=1024,
                            activation=tf.nn.relu)

    print"features: ",features.get_shape()
    print"================================="
    print"================================="

    return features

def DenseNet(x,drop_rate, height, width, n_classes):
    print"x:", x.get_shape()

    #reshape to 4D tensor
    input_layer = tf.reshape(x, [-1, height, width, 1])
    print"input_layer:", input_layer.get_shape()

    outCBP = ConvBnPool(input_layer)
    print"outCBP:", outCBP.get_shape()

    outDense = None

    for i in range(6):

        if i==0:
            outDense = DenseBlock(outCBP,drop_rate,i)
            print "\noutDense_first:", outDense.get_shape()
            #outDense_first = tf.concat([outCBP,outDense_first], 3)

        else:
            temp_outDense = DenseBlock(outDense,drop_rate,i)
            print"\noutDense: ", outDense.get_shape()
            outDense = tf.concat([outDense,temp_outDense], 3)


    print"\noutDense: ", outDense.get_shape()
    features = ClassificationLayer( outDense )

    logits = tf.layers.dense(features, units=n_classes)

    return logits,features


def train_nn(x, height, width, n_classes):

    sess = tf.InteractiveSession()

    prediction, feat = DenseNet(x,drop_rate, height, width, n_classes)
    print"\n=======TRAIN==========="

    print"prediction: ", prediction.get_shape()
    print"feat: ", feat.get_shape()


    tensor1d=tf.nn.softmax_cross_entropy_with_logits(labels=y,logits=prediction)
    cost = tf.reduce_mean(tensor1d)
    optimizer=tf.train.AdamOptimizer().minimize(cost)

    correct = tf.equal(tf.argmax(prediction,1),tf.argmax(y,1))
    accuracy = tf.reduce_mean(tf.cast(correct,tf.float64))

    #tf.summary.scalar("cost_function",cost)
    #writer = tf.summary.FileWriter("./output/histogram_example")
    #summaries = tf.summary.merge_all()
    tf.global_variables_initializer().run()

    #saver = tf.train.Saver()
    iterations = int(mnist.train.num_examples/batch_size)

    hm_epochs=5

    j = 0
    for epoch in range(hm_epochs):

        lossi=0
        accS = 0

        for _ in range(iterations):
            batch_x,batch_y = mnist.train.next_batch(batch_size)
            #summ, _, c=sess.run([summaries,optimizer, cost],feed_dict={x: batch_x, y: batch_y})
            acc,_, c=sess.run([accuracy,optimizer, cost],feed_dict={x: batch_x, y: batch_y, drop_rate:0.8})
            lossi+=c
            accS+=acc
            #writer.add_summary(summ,j)
            #writer.flush()
            j+=1

        print "Train loss:",lossi/iterations,"| accuracy:",accS/iterations
    correct = tf.equal(tf.argmax(prediction, 1),tf.argmax(y,1))
    accuracy = tf.reduce_mean(tf.cast(correct,'float'))

    #saver.save(sess, './output/model')
    #writer.close()
    print'accuracy:', accuracy.eval({x:mnist.test.images, y:mnist.test.labels})


mnist = input_data.read_data_sets("/tmp/data/", one_hot = True)

height = 28
width = 28
batch_size = 128
n_classes = 10

#Graph input
drop_rate = tf.placeholder( tf.float32, [], name = "drop_rate" )
x = tf.placeholder( tf.float32, [ None, height*width ], name = "x" )
y = tf.placeholder( tf.float32, [ None, n_classes ], name = "y" )

train_nn(x, height, width, n_classes)
