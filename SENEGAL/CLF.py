from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_recall_fscore_support
import os
import numpy as np
import sys

p_split = 70
n_split=10

#python CLF RFC,SVC,GBC 1

arr_C=sys.argv[1]
norm=int(sys.argv[2])


for class_ in arr_C:
    directory = './dataset/N%d/%s_truthpred_%d%s'%(norm,class_,p_split,'p')
    if not os.path.exists(directory):
        os.makedirs(directory)


    for i in range(n_split):
        print ("\n\t---iter:",i,"----")
        var_train_x = './dataset/N%d/train_x%d%s%d%s'%(norm,i,'_',p_split,'.npy')
        var_train_y = './dataset/N%d/train_y%d%s%d%s'%(norm,i,'_',p_split,'.npy')
        var_test_x = './dataset/N%d/test_x%d%s%d%s'%(norm,i,'_',p_split,'.npy')
        var_test_y = './dataset/N%d/test_y%d%s%d%s'%(norm,i,'_',p_split,'.npy')


        train_x = np.load(var_train_x)
        train_y = np.load(var_train_y)
        test_x = np.load(var_test_x)
        test_y = np.load(var_test_y)

        #TRAIN WITH RANDOM FOREST
        if class_=='RFC':
            clf = RandomForestClassifier()
            clf.fit(train_x,train_y)
            predC = clf.predict(test_x)
        if class_=='GBC':
            clf = GradientBoostingClassifier(learning_rate=0.001)
            clf.fit(train_x, train_y)
            predC = clf.predict(test_x)
        if class_=='SVC':
            clf = SVC()
            clf.fit(train_x, train_y)
            predC = clf.predict(test_x)

        var_totpred = './dataset/N%d/%s_truthpred_70p/totpred%d'%(norm,class_,i)
        var_gt='./dataset/N%d/%s_truthpred_70p/gt%d'%(norm,class_,i)
        np.save(var_totpred, predC)
        np.save(var_gt, test_y)
