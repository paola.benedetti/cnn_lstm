import sys
import tensorflow as tf
import numpy as np
from sklearn.metrics import accuracy_score


#Format values matrix from (n,22*13) shape to (n,22,13)
def getRNNFormat(X):

    #print X.shape
    new_X = []
    for row in X:
        new_X.append( np.split(row, 22) )
    return np.array(new_X)

#Format labels matrix from (n,1) shape to (n,3)
def getRNNFormatLabel(Y):

    vals = np.unique(np.array(Y))
    sorted(vals)
    hash_val = {}

    for el in vals:
        hash_val[el] = len(hash_val.keys())

    new_Y = []

    for el in Y:
        t = np.zeros(len(vals))
        t[hash_val[el]] = 1.0
        new_Y.append(t)

    return np.array(new_Y)

#Get i-th batches of values set X and labels set Y
def getBatch2(X, Y, i, batch_size):
    start_id = i*batch_size
    end_id = min( (i+1) * batch_size, X.shape[0])

    batch_x = X[start_id:end_id]
    batch_y = Y[start_id:end_id]

    return batch_x, batch_y

def checkTest( data, labels, batch_size, sess ):
    accS=0
    tot_pred = []
    iterations = len(data) / batch_size
    if len(data) % batch_size != 0:
        iterations+=1

    cl = np.zeros(len(data))

    for ibatch in range(iterations):

        #BATCH_X BATCH_Y: i-th batches of test_x and test_y
        ba_x, batch_y = getBatch2(data, cl, ibatch, batch_size)

        #PRED_TEMP: (n,3)
        pred_temp = sess.run(prediction,feed_dict={x:ba_x})
        #TOT_PRED: accumulate max argument of each test prediciton evaluated by batch
        #TOT_PRED: (n,1)
        for el in pred_temp:
            tot_pred.append( np.argmax(el) )

    #GT: contains max argument of each test ground truth
    # test_y (n,3) -> gt (n,1)
    gt = []
    for el in labels:
        gt.append( np.argmax(el))

    tot_pred = tot_pred[0:len(gt)]

    test_acc = accuracy_score(gt, tot_pred)
    print "TEST Accuracy ", test_acc
    return test_acc


p_split=60

batchsz=int(sys.argv[1])
nunits=int(sys.argv[2])
epochs = int(sys.argv[3])
nlayer=int(sys.argv[4])
norm=int(sys.argv[5])
itr =int(sys.argv[6])
tag = sys.argv[7]

p_train=str(60)
p_val=str(20)

g_dir = "../dataset/N%d" % norm


path_in_dir = g_dir+'/%s_%dl_truthpred_%dp%du%db/models%d'%(tag,nlayer,p_split,nunits,batchsz,itr)
path_in_model = path_in_dir+'/my-model-%d.meta' % itr

path_in_x = g_dir+'/dati_m/TimeSeries/test_x%d_%s-%s.npy'%(itr,p_train,p_val)
path_in_y = g_dir+'/dati_m/ground_truth/test_y%d_%s-%s.npy'%(itr,p_train,p_val)

f = open(g_dir+'/%s_%dl_truthpred_%dp%du%db/TESTAccuracy.txt'%(tag, nlayer,p_split,nunits,batchsz), 'a')
f.write("%d\t"%itr)
tf.reset_default_graph()

with tf.Session() as sess:
    model_saver = tf.train.import_meta_graph(path_in_model)
    model_saver.restore(sess, tf.train.latest_checkpoint(path_in_dir))
    print "Model Restored"

    graph = tf.get_default_graph()
    x = graph.get_tensor_by_name("x:0")
    y = graph.get_tensor_by_name("y:0")
    prediction = graph.get_tensor_by_name("prediction:0")

    test_x = np.load(path_in_x)
    test_y = np.load(path_in_y)

    test_x = getRNNFormat(test_x)
    test_y = getRNNFormatLabel(test_y)

    test_acc = checkTest( test_x, test_y, batchsz, sess )
    f.write(str(test_acc)+'\n')
f.close()
