import sys
import os
import gdal
import numpy as np
import math
from operator import itemgetter, attrgetter, methodcaller
import tensorflow as tf
import random


#get from .tif those pixel having label != 0 (:backgournd)
#return matrix n_pixel*label  as ((px,py),label)
#first element of the matrix contains coordinates of the pixel while second element contains label value
def getDataLabel(s):
	print "read %s"%s
	dataSet = gdal.Open(s)
	for band in range(1, dataSet.RasterCount+1 ):

		m = dataSet.GetRasterBand(band).ReadAsArray()
		a,b = m.shape

		#bin count of the matrix
        ar = np.reshape(m,m.shape[0]*m.shape[1])

		#get only those classes represented by a cospicuous amount of pixel
		#bitcount find 9 classes but only 3 are represented by enougth pixels
		#we maintain only those classes represented by an amount of pixel >100
        classes = np.argwhere(np.bincount(ar)>100)

        sz,_=classes.shape
        out = []
        x = []
        y = []
        c=0

		#get pixel having label !=0 (not background)
		#and having a relevant classification (one of those classes with pixel amount>100)
        for i in range(a):
            for j in range(b):
                for k in range(sz):
                    if m[i,j]==classes[k,0]:
                        if m[i,j]!=0:
                            x.append(i)
                            y.append(j)
                            out.append([i,j,m[i,j],0])
	return np.array(out),np.array(x),np.array(y),sz-1

def getSecondMin(mnew):

	min_ = np.amin(mnew)

	for el in mnew:
		indexes = np.where(el==min_)



#Normalization to avoid saturation (constant accuracy values) while performing training
def normalizeB(mnew):
	max_ = np.amax(mnew)
	min_first = np.amin(mnew)
	print "======================"
	print "shape band matix",mnew.shape
	print "max:",max_
	print "first min",min_first


	if max_ == min_first:
		return mnew * 0
	else:
		min_second = np.amin(np.array(mnew)[mnew != min_first])
		print "second min:",min_second
		print "======================"
		#min_ = np.amin(mnew)
		#return ((mnew.astype('float32') - min_) / (max_ - min_) * 512) - 256
		return ((mnew.astype('float32') - min_second)/(max_ - min_second))

#get pixel values by band from .tif
#return matrix n_pixel*band  as ((px,py),band)
#Values are normalized by band
def getDataImages1(s):

	ds_img = []
	dsnew = gdal.Open(s)

	for band in range(1,dsnew.RasterCount+1):
		mnew = dsnew.GetRasterBand(band).ReadAsArray()
		mnew = normalizeB(mnew)

		ds_img.append(mnew)


	print "\nmin:",np.amin(ds_img)
	print "max:",np.amax(ds_img)
	return np.array(ds_img)

#get pixel values by band from .tif
#return matrix n_pixel*band  as ((px,py),band)
#Values are normalized by band pover all 22 timestamps
def getDataImages2(s,time_stamps):

	ds_img = []
	dsnew = gdal.Open(s)

	ds=[]

	#start from index 1
	#For each band get max and min values over all 22 timestsmps
	max_bands=[sys.float_info.min] * (time_stamps)
	min_bands=[sys.float_info.max] * (time_stamps)

	for band in range(1,dsnew.RasterCount+1):
		mnew = dsnew.GetRasterBand(band).ReadAsArray()


		print "\n Band:",band
		max_=np.amax(mnew)
		print "MAx:",max_
		min_first=np.amin(mnew)
		print "MIN first:",min_first

		if max_ == min_first:
			min_second=np.amin(mnew)
		else:
			min_second = np.amin(np.array(mnew)[mnew != min_first])
		print "MIN second:",min_second
		print "==================================="



		if max_bands[ (band-1) % time_stamps ] < max_:
			max_bands[ (band-1) % time_stamps ] = max_

		if min_bands[ (band-1) % time_stamps ] > min_second:
			min_bands[ (band-1) % time_stamps ] = min_second

		ds_img.append(mnew)

	ds_n = normalizeBT(ds_img,max_bands,min_bands,time_stamps)

	print "max=",np.amax(ds_n)
	print "min=",np.amin(ds_n)

	return ds_n

#normalize each band by the max and min values found over all 22 timestamps
def normalizeBT(ds_img,max_bands,min_bands,time_stamps):
	#length=len(ds_img)
	#time_stamps = 13
	ds_n=[]

	for i in range(len(ds_img)):
		t_matrix = ds_img[i].astype('float32')
		t_matrix = ( t_matrix - min_bands[ i % time_stamps ] ) / (max_bands[ i % time_stamps] - min_bands[i%time_stamps])

		#t_matrix = (t_matrix * 512) - 256
		#print "max %f - min %f " % (np.amax(t_matrix), np.amin(t_matrix))
		ds_n.append(t_matrix)
	return ds_n


#get pairwise eucludean distance for each pixel in the dataSet
#return the distance matrix
def getDists(ds,x,y):

    a,_ = ds.shape
    dist = []
    Edists = []

    i=0
    while i<a:
        for j in range(a):
            if j<=i:
                dist.insert(j,0)
            else:
                d = int (math.sqrt( (x[i]-x[j])**2 + (y[i]-y[j])**2 ))
                dist.insert(j,d)

        #Edists: peirwise distances for each pixel
        Edists.insert(i,dist)

        dist = []
        i+=1
    return Edists


def getMin_i(i,i_dists):
    min_=1000000
    i_min=0
    i=0
    for el in i_dists:
        if (el<min_) & (el!=0):
            min_=el
            i_min=i
        i+=1

    return i_min


def getObj(Edists,ds_label):

    print np.array(Edists).shape
    print np.array(ds_label).shape
	#1324, 1426

    obj=0

    for i in range(len(Edists)):

		#pairwise pixel having minimum distance and havinf same label will be contained in the same object
        min_i=getMin_i(i,Edists[i])

        if ds_label[i][2]==ds_label[min_i][2]:
            if (ds_label[i][3]!=0) & (ds_label[min_i][3]==0):
                ds_label[min_i][3]=ds_label[i][3]
            if (ds_label[i][3]==0) & (ds_label[min_i][3]!=0):
                ds_label[i][3]=ds_label[min_i][3]
            if (ds_label[i][3]==0) & (ds_label[min_i][3]==0):
                obj=obj+1
                ds_label[i][3]=obj
                ds_label[min_i][3]=obj

	#those pixel left are considered part of a same last object
    obj+=1
    for i in range(len(ds_label)):
        if ds_label[i][3]==0:
            ds_label[i][3]=obj

    #print "obj: ",obj

    return ds_label,obj

#Merge values and label data set
def mergeDS(ds_label,ds_img):

    ds_merged = []

    row_label,_ = ds_label.shape
    row_img,h,k = np.array(ds_img).shape

    for i in range(row_label):
        n = 0
        el = []
        pixel_x = ds_label[i][0]
        pixel_y = ds_label[i][1]


        #DS MERGED: will be  [values, label, obj]
		#add values
        for j in range(row_img):
            el.insert(n,ds_img[j][pixel_x][pixel_y])
            n+=1

        #add labels and objects
        el.insert(n,ds_label[i][2])
        n+=1
        el.insert(n,ds_label[i][3])

        #add new raw in mergedDS
        ds_merged.insert(i,el)

    return np.array(ds_merged)


def getData(s_label,s_img,time_stamps, TAG):

	#DS_LABEL: [px,py,label,obj]
    ds_label,x,y,classes = getDataLabel(s_label)
    print "getDataLabel DONE"
    print np.array(ds_label).shape

    #get pairwise pixel Euclidean distance and returns Edists
    #Edists:px,py,dists(p,q),label
    Edists = getDists(ds_label,x,y)
    print "getDists DONE"

	#DS_LABEL: [px, pj, label, obj]
    #OBJ: number og objects found in the DS
    #Assign to each pixel the object of membership
    ds_label,obj = getObj(Edists,ds_label)
    print "oggetti:",obj
    print "getObj DONE"

	#DS_IMG: 3dim [band_values,px,py], first element is composed of the pixel values for each band
	#N1: band normalization
	#N2: band over timestamps normalization
    if TAG=='N1':
        ds_img=getDataImages1(s_img)
        print "fetDataImages DONE"
        print np.array(ds_img).shape
    if TAG=='N2':
        ds_img=getDataImages2(s_img,time_stamps)
        print "fetDataImages DONE"
        print np.array(ds_img).shape

	#DS:pi,pj,vals(286),label,obj
    ds=mergeDS(ds_label,ds_img)
    print "mergeDS DONE"

    return ds,ds_img,ds_label, classes

#N1 or N2 depends on which normalization we want to perfor
TAG_=sys.argv[1]
p_split=70
n_split=10
ninput=13
time_stamps = 13

g_path_out = '../../dataset/%s/dati_m/'%TAG_
g_path_in = '../../DATA/'

s_img=g_path_in+'IMG_DATA/LANDSAT_INDEX_PANSHARP_FMASK_10-10-0-5_GAPFILL_LINEAR_DATELIST.TIF'
s_label=g_path_in+'TRAINING_DATA/TRAINING_15M.TIF'

ds,ds_img,ds_label,nclasses = getData(s_label, s_img,time_stamps, TAG_)


var_ds_img = g_path_out+'TimeSeries/dsLR_img.npy'
var_ds_label = g_path_out+'orig/ds_label.npy'
var_ds = g_path_out+'orig/ds.npy'

print "dsLR_img:",np.array(ds_img).shape
print "ds_label:",np.array(ds_label).shape
print "ds:",np.array(ds).shape

np.save(var_ds_img,ds_img)
np.save(var_ds_label,ds_label)
np.save(var_ds,ds)
