import sys
import os
import numpy as np
from datetime import datetime
import random

def writeSplit(idx, data, outFileName ):
    fold = []
    print data.shape
    print np.amax(idx)
    print np.amin(idx)
    for i in idx:
        fold.append( data[i,0:-2] )
    fold = np.array(fold)
    np.save(outFileName, fold )

def getIdxSplit(hash_class, perc_train, perc_validation):
    train_idx = []
    validation_idx = []
    test_idx = []
    dt = datetime.now()
    random.seed( dt.microsecond )
    classes = hash_class.keys()

    for cl in classes:
        vals = hash_class[cl].keys()
        random.shuffle(vals)
        thr = int(len(vals) * perc_train)
        vhr = int(len(vals) * perc_validation)
        print "%d over %d" % (thr, len(vals))
        print "%d over %d" % (vhr, len(vals))
        for i in range(len(vals)):
            if i < thr:
                train_idx.extend( hash_class[cl][vals[i]] )

            if (i >= thr) & (i<vhr+thr):
                validation_idx.extend( hash_class[cl][vals[i]] )

            if i>=vhr+thr:
                test_idx.extend( hash_class[cl][vals[i]] )

    print "============"

    #print "\n"
    #print "train_idx:",len(train_idx)
    #print "validation_idx:",len(validation_idx)
    #print "test_idx:",len(test_idx)
    #print "tot:",len(train_idx)+len(validation_idx)+len(test_idx)

    return train_idx, validation_idx, test_idx




norm = sys.argv[1]
perc_train = float(sys.argv[2])
perc_validation = float(sys.argv[3])

path_in = "dataset/%s/dati_m/orig"%norm
path_out = "dataset/%s/dati_m"%norm

ds_label = np.load( path_in+"/ds_label.npy" )
#vhsr = np.load( path_in+"/newHR_img.npy" )
timeSeries = np.load( path_in+"/ds.npy" )

print ds_label.shape
print timeSeries.shape

#split_id = 0


hash_class = {}
pid = 0
for el in ds_label:
	cl = el[2]
	obj_id = el[3]
	if cl not in hash_class:
		hash_class[cl] = {}

	if obj_id not in hash_class[cl]:
		hash_class[cl][obj_id] = []
	hash_class[cl][obj_id].append( pid )
	pid = pid + 1

print "percentage train: %f"%perc_train
print "percentage validation: %f"%perc_validation
print "percentage test: %f"%float(1-(perc_train+perc_validation))


for split_id in range(30):
    train_idx, validation_idx, test_idx = getIdxSplit(hash_class, perc_train, perc_validation)

    #write time series splits
    if not os.path.exists(path_out+"/TimeSeries"):
        os.makedirs(path_out+"/TimeSeries")

    outFileTrain = path_out+"/TimeSeries/train_x"+str(split_id)+"_"+str(int(perc_train*100))+"-"+str(int(perc_validation*100))+".npy"
    writeSplit(train_idx, timeSeries, outFileTrain )

    outFileTrain = path_out+"/TimeSeries/validation_x"+str(split_id)+"_"+str(int(perc_train*100))+"-"+str(int(perc_validation*100))+".npy"
    writeSplit(validation_idx, timeSeries, outFileTrain )

    outFileTrain = path_out+"/TimeSeries/test_x"+str(split_id)+"_"+str(int(perc_train*100))+"-"+str(int(perc_validation*100))+".npy"
    writeSplit(test_idx, timeSeries, outFileTrain )


	#write vhsr splits
	#outFileTrain = path_out+"/VHSR/train_x"+str(split_id)+"_"+str(int(perc_train*100))+".npy"
	#writeSplit(train_idx, vhsr, outFileTrain )

	#outFileTest = path_out+"/VHSR/test_x"+str(split_id)+"_"+str(int(perc_train*100))+".npy"
	#writeSplit(test_idx, vhsr, outFileTest )

    if not os.path.exists(path_out+"/ground_truth"):
        os.makedirs(path_out+"/ground_truth")

    outFileTrain = path_out+"/ground_truth/train_y"+str(split_id)+"_"+str(int(perc_train*100))+"-"+str(int(perc_validation*100))+".npy"
    np.save(outFileTrain, np.array( ds_label[train_idx, 2 ] ))

    outFileTrain = path_out+"/ground_truth/validation_y"+str(split_id)+"_"+str(int(perc_train*100))+"-"+str(int(perc_validation*100))+".npy"
    np.save(outFileTrain, np.array( ds_label[validation_idx, 2 ] ))

    outFileTrain = path_out+"/ground_truth/test_y"+str(split_id)+"_"+str(int(perc_train*100))+"-"+str(int(perc_validation*100))+".npy"
    np.save(outFileTrain, np.array( ds_label[test_idx, 2 ] ))
