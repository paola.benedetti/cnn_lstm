import sys
import os
import numpy as np
import math
from operator import itemgetter, attrgetter, methodcaller
import tensorflow as tf
from tensorflow.contrib import rnn
import random
from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_recall_fscore_support
from sklearn.metrics import f1_score
from sklearn.ensemble import RandomForestClassifier

def CNN(x, nunits):
	conv1 = tf.layers.conv2d(
	      inputs=x,
	      filters=32,
	      kernel_size=[3, 3],
	      padding="valid",
	      activation=tf.nn.relu)
	
	conv1 = tf.layers.batch_normalization(conv1)
	
	pool1 = tf.layers.max_pooling2d(inputs=conv1, pool_size=[2, 2], strides=2)
	
	conv2 = tf.layers.conv2d(
	      inputs=conv1,
	      filters=nunits,
	      kernel_size=[3, 3],
	      padding="valid",
	      activation=tf.nn.relu)
	'''
	conv2 = tf.layers.batch_normalization(conv2)
	pool2 = tf.layers.max_pooling2d(inputs=conv2, pool_size=[2, 2], strides=2)
	
	_, nrow, ncol, nfilters = pool2.get_shape()
	print "nrow %d" % nrow
	print "ncol %d" % ncol
	print "nfilters %d" % nfilters
	'''
	cnn = tf.reduce_mean(conv2, [1,2])
	#cnn = tf.nn.tanh(cnn)
	return cnn
	
def RnnAttention(x, nunits, nlayer):
	x = tf.unstack(x,22,1)
	
	#NETWORK DEF
	#MORE THEN ONE LAYER: list of LSTMcell,nunits hidden units each, for each layer
	if nlayer>1:
		cells=[]
		for _ in range(nlayer):
			cell = rnn.LSTMCell(nunits)
			#cell = rnn.LSTMCell(nunits)
			cells.append(cell)
		cell = tf.contrib.rnn.MultiRNNCell(cells)
    #SIGNLE LAYER: single GRUCell, nunits hidden units each
	else:
		cell = rnn.LSTMCell(nunits)
		#cell = rnn.LSTMCell(nunits)
	outputs,_=rnn.static_rnn(cell, x, dtype="float32")
	
	outputs = tf.stack(outputs, axis=1)
	print outputs.get_shape()
	
	# Trainable parameters
	attention_size = nunits
	W_omega = tf.Variable(tf.random_normal([nunits, attention_size], stddev=0.1))
	b_omega = tf.Variable(tf.random_normal([attention_size], stddev=0.1))
	u_omega = tf.Variable(tf.random_normal([attention_size], stddev=0.1))

	# Applying fully connected layer with non-linear activation to each of the B*T timestamps;
	#  the shape of `v` is (B,T,D)*(D,A)=(B,T,A), where A=attention_size
	v = tf.tanh(tf.tensordot(outputs, W_omega, axes=1) + b_omega)
	# For each of the timestamps its vector of size A from `v` is reduced with `u` vector
	vu = tf.tensordot(v, u_omega, axes=1)   # (B,T) shape
	alphas = tf.nn.softmax(vu)              # (B,T) shape also

	# Output of (Bi-)RNN is reduced with attention vector; the result has (B,D) shape
	output = tf.reduce_sum(outputs * tf.expand_dims(alphas, -1), 1)
	return output

def BiRNN(x, nunits, nlayer):
	#PLACEHOLDERS + WEIGHT & BIAS DEF
    #Processing input tensor
	x = tf.unstack(x,22,1)
	
	#NETWORK DEF
	#MORE THEN ONE LAYER: list of LSTMcell,nunits hidden units each, for each layer
	b_cell = None
	f_cell = None
	if nlayer>1:
		f_cells=[]
		b_cells=[]
		for _ in range(nlayer):
			f_cells.append( rnn.LSTMCell(nunits) )
			b_cells.append( rnn.LSTMCell(nunits) )
		
		b_cell = tf.contrib.rnn.MultiRNNCell(b_cells)
		f_cell = tf.contrib.rnn.MultiRNNCell(f_cells)
	#SIGNLE LAYER: single GRUCell, nunits hidden units each
	else:
		b_cell = rnn.LSTMCell(nunits)
		f_cell = rnn.LSTMCell(nunits)
	outputs,_,_=rnn.static_bidirectional_rnn(f_cell, b_cell, x, dtype=tf.float32)
	return outputs[-1]


def Rnn(x, nunits, nlayer):
	#PLACEHOLDERS + WEIGHT & BIAS DEF
    #Processing input tensor
	x = tf.unstack(x,22,1)
	
	#NETWORK DEF
	#MORE THEN ONE LAYER: list of LSTMcell,nunits hidden units each, for each layer
	if nlayer>1:
		cells=[]
		for _ in range(nlayer):
			cell = rnn.LSTMCell(nunits)
			#cell = rnn.LSTMCell(nunits)
			cells.append(cell)
		cell = tf.contrib.rnn.MultiRNNCell(cells)
    #SIGNLE LAYER: single GRUCell, nunits hidden units each
	else:
		cell = rnn.LSTMCell(nunits)
		#cell = rnn.LSTMCell(nunits)
	outputs,_=rnn.static_rnn(cell, x, dtype="float32")
	return outputs[-1]

def RnnFull(x, nunits, nlayer):
	n_timetamps = 22
	#PLACEHOLDERS + WEIGHT & BIAS DEF
	#Processing input tensor
	x = tf.unstack(x,n_timetamps,1)

	#NETWORK DEF
	#MORE THEN ONE LAYER: list of LSTMcell,nunits hidden units each, for each layer
	if nlayer>1:
		cells=[]
		for _ in range(nlayer):
			cell = rnn.LSTMCell(nunits)
			#cell = rnn.LSTMCell(nunits)
			cells.append(cell)
		cell = tf.contrib.rnn.MultiRNNCell(cells)
	#SIGNLE LAYER: single GRUCell, nunits hidden units each
	else:
		cell = rnn.LSTMCell(nunits)
		#cell = rnn.LSTMCell(nunits)
	outputs,_=rnn.static_rnn(cell, x, dtype="float32")
	
	features = tf.reshape(outputs, [-1, nunits*n_timetamps])
	print features.get_shape()
	return features #outputs[-1]





def getBatch(X, Y, i, batch_size):
    start_id = i*batch_size
    end_id = min( (i+1) * batch_size, X.shape[0])
    batch_x = X[start_id:end_id]
    batch_y = Y[start_id:end_id]
    return batch_x, batch_y

def getLabelFormat(Y):
	vals = np.unique(np.array(Y))
	sorted(vals)
	hash_val = {}
	for el in vals:
		hash_val[el] = len(hash_val.keys())
	new_Y = []
	for el in Y:
		t = np.zeros(len(vals))
		t[hash_val[el]] = 1.0
		new_Y.append(t)
	return np.array(new_Y)

def getRNNFormat(X):
    #print X.shape
    new_X = []
    for row in X:
        new_X.append( np.split(row, 22) )
    return np.array(new_X)


def getPrediction(x_rnn, x_cnn, nunits, nlayer, nclasses, choice):
	n_timetamps = 22
	outb = tf.Variable(tf.truncated_normal([nclasses]),name='B')
	
	if choice == "BIRNN":
		outputs = BiRNN(x_rnn, nunits, nlayer)
		first_dim = nunits*2
		
	if choice == "RNN":
		outputs = Rnn(x_rnn, nunits, nlayer)
		first_dim = nunits
		
	if choice == "CNN":
		outputs = CNN(x_cnn, nunits)
		first_dim = nunits
		
	if choice == "CNN_RNN":
		vec_rnn = Rnn(x_rnn, nunits, nlayer)
		vec_cnn = CNN(x_cnn, nunits)
		outputs=tf.concat([vec_rnn,vec_cnn],1)
		first_dim = nunits*2
		
	if choice == "CNN_BIRNN":
		vec_rnn = BiRNN(x_rnn, nunits, nlayer)
		vec_cnn = CNN(x_cnn, nunits)
		outputs=tf.concat([vec_rnn,vec_cnn],1)
		first_dim = nunits*3
	
	outw = tf.Variable(tf.truncated_normal([first_dim,nclasses]),name='W')	

	prediction = tf.matmul(outputs,outw,name='prediction')+outb

	return prediction


nclasses = 3
nunits = 16
batchsz = 8
hm_epochs = 50
n_levels_lstm = 3

ts_train = np.load(sys.argv[1])
vhsr_train = np.load(sys.argv[2])
label_train = np.load(sys.argv[3])
print np.bincount(label_train)

ts_test = np.load(sys.argv[4])
vhsr_test = np.load(sys.argv[5])
label_test = np.load(sys.argv[6])
choice = sys.argv[7]
outputFileName = sys.argv[8]


x_rnn = tf.placeholder("float",[None,22,13],name="x_rnn")
x_cnn = tf.placeholder("float",[None,30,30,4],name="x_cnn")

y = tf.placeholder("float",[None,nclasses],name="y")
learning_rate = tf.placeholder(tf.float32, shape=[])

sess = tf.InteractiveSession()

prediction = getPrediction(x_rnn, x_cnn, nunits, n_levels_lstm, nclasses, choice)

tensor1d = tf.nn.softmax_cross_entropy_with_logits(labels=y,logits=prediction)
cost = tf.reduce_mean(tensor1d)

optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(cost)

correct = tf.equal(tf.argmax(prediction,1),tf.argmax(y,1))
accuracy = tf.reduce_mean(tf.cast(correct,tf.float64))

tf.global_variables_initializer().run()



train_rf = np.reshape( vhsr_train, (vhsr_train.shape[0], vhsr_train.shape[1] * vhsr_train.shape[2] * vhsr_train.shape[3] ) )
#train_rf = np.column_stack( (train_rf, ts_train) )

#train_rf = ts_train

clf = RandomForestClassifier(n_estimators = 100)
clf.fit(train_rf, label_train)




classes = label_train
rnn_data_train = getRNNFormat(ts_train)
train_y = getLabelFormat(classes)


iterations = rnn_data_train.shape[0] / batchsz

if rnn_data_train.shape[0] % batchsz != 0:
    iterations+=1

for e in range(hm_epochs):
	lossi = 0
	accS = 0
	for ibatch in range(iterations):
		#BATCH_X BATCH_Y: i-th batches of train_indices_x and train_y
		batch_rnn_x, batch_y = getBatch(rnn_data_train, train_y, ibatch, batchsz)

		#BATCH_X BATCH_Y: i-th batches of train_raw_x and train_y
		batch_cnn_x, batch_y = getBatch(vhsr_train, train_y, ibatch, batchsz)
		batch_cnn_x = np.swapaxes(batch_cnn_x,1,3)
		
		acc,_,loss = sess.run([accuracy,optimizer,cost],feed_dict={x_rnn:batch_rnn_x, 
																	x_cnn:batch_cnn_x,
																	y:batch_y, 
																	learning_rate:0.001})		
		lossi+=loss
		accS+=acc
	
	print "Epoch:",e,"Train loss:",lossi/iterations,"| accuracy:",accS/iterations
	
	#media accuracy su 35 batches
	#print TAG,":Epoch",e,"/",hm_epochs,",avg",iterations,"batches-->loss:", lossi/iterations,"| accuracy:",accS/iterations
	


classes_test = label_test
rnn_data_test = getRNNFormat(ts_test)
test_y = getLabelFormat(classes_test)



tot_pred = []
gt_test = []

'''
iterations = rnn_data_test.shape[0] / batchsz

if rnn_data_test.shape[0] % batchsz != 0:
    iterations+=1

for ibatch in range(iterations):
	batch_rnn_x, batch_y = getBatch(rnn_data_test, test_y, ibatch, batchsz)
	
	batch_cnn_x, batch_y = getBatch(vhsr_test, test_y, ibatch, batchsz)
	
	batch_cnn_x = np.swapaxes(batch_cnn_x,1,3)
	pred_temp = sess.run(prediction,feed_dict={x_rnn:batch_rnn_x, 
												x_cnn:batch_cnn_x})
	
	#TOT_PRED: accumulate max argument of each test prediciton evaluated by batch
	#TOT_PRED: (n,1)
	for el in pred_temp:
		tot_pred.append( np.argmax(el) )
	
	for el in batch_y:
		gt_test.append( np.argmax(el) )
'''

vhsr_test = np.swapaxes(vhsr_test,1,3)
pred = sess.run(prediction,feed_dict={x_rnn:rnn_data_test, x_cnn:vhsr_test})

for el in test_y:
	gt_test.append( np.argmax(el) )

for el in pred:
	tot_pred.append( np.argmax(el) )
	
print "COMBO TEST F-Measure: %f" % f1_score(gt_test, tot_pred, average='weighted')
print f1_score(gt_test, tot_pred, average=None)
print "COMBO TEST Accuracy: %f" % accuracy_score(gt_test, tot_pred)
print "============================="
	
	
''''
print "gt_test"
print len(gt_test)

print "tot_pred"
print len(tot_pred)
print "======"


print rnn_data_test.shape
print vhsr_test.shape

print "COMBO TEST F-Measure: %f" % f1_score(gt_test, tot_pred, average='weighted')
print f1_score(gt_test, tot_pred, average=None)
print "COMBO TEST Accuracy: %f" % accuracy_score(gt_test, tot_pred)

#test_rf = np.reshape( vhsr_test, (vhsr_test.shape[0], vhsr_test.shape[1] * vhsr_test.shape[2] * vhsr_test.shape[3] ) )
#test_rf = np.column_stack( (test_rf, ts_test) )
#test_rf = ts_test

#clf = RandomForestClassifier(n_estimators = 100)
#clf.fit(train_rf, label_train)

rf_classif = clf.predict(test_rf)

#print "RF TEST F-Measure: %f" % f1_score(label_test, rf_classif, average='weighted')
#print f1_score(label_test, rf_classif, average=None)
#print "RF TEST Accuracy: %f" % accuracy_score(label_test, rf_classif)
'''

test_rf = np.reshape( vhsr_test, (vhsr_test.shape[0], vhsr_test.shape[1] * vhsr_test.shape[2] * vhsr_test.shape[3] ) )
rf_classif = clf.predict(test_rf)
print "RF TEST F-Measure: %f" % f1_score(label_test, rf_classif, average='weighted')
print f1_score(label_test, rf_classif, average=None)
print "RF TEST Accuracy: %f" % accuracy_score(label_test, rf_classif)


np.save(outputFileName, np.array(tot_pred))


'''
#GT: contains max argument of each test ground truth
# test_y (n,3) -> gt (n,1)
gt = []
for el in test_y:
    gt.append( np.argmax(el))

tot_pred = tot_pred[0:len(gt)]
print "Accuracy ", accuracy_score(gt, tot_pred)

#SAVE GROUD TRUTH gt E PREDICTION tot_pred
var_totpred = '%s%s%d%s'%(directory,'/totpred',itr,'.npy')
var_gt='%s%s%d%s'%(directory,'/gt',itr,'.npy')
np.save(var_totpred, tot_pred)
np.save(var_gt, gt)

'''
