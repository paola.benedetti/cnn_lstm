import sys
import os
import gdal
import numpy as np
from operator import itemgetter, attrgetter, methodcaller
import tensorflow as tf
from tensorflow.contrib import rnn
import random
from sklearn.utils import shuffle

from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_recall_fscore_support




#(n,img*timestep)->(n,img,timestep)
def split(arr, size):

     arrs = []

     while len(arr) > size:

         pice = arr[:size]
         arrs.append(pice)
         arr = arr[size:]

     arrs.append(arr)

     return arrs

#Format values matrix from (n,22*13) shape to (n,22,13)
def getRNNFormat(X):

    #print X.shape
    new_X = []
    for row in X:
        new_X.append( np.split(row, 22) )
    return np.array(new_X)

#Format labels matrix from (n,1) shape to (n,3)
def getRNNFormatLabel(Y):

    vals = np.unique(np.array(Y))
    sorted(vals)
    hash_val = {}

    for el in vals:
        hash_val[el] = len(hash_val.keys())

    new_Y = []

    for el in Y:
        t = np.zeros(len(vals))
        t[hash_val[el]] = 1.0
        new_Y.append(t)

    return np.array(new_Y)

#Get i-th batches of values set X and labels set Y
def getBatch2(X, Y, i, batch_size):
    start_id = i*batch_size
    end_id = min( (i+1) * batch_size, X.shape[0])

    batch_x = X[start_id:end_id]
    batch_y = Y[start_id:end_id]

    return batch_x, batch_y


def RnnAttention(x, nunits, nlayer, n_timetamps):
    	x = tf.unstack(x,n_timetamps,1)
    	#NETWORK DEF
    	#MORE THEN ONE LAYER: list of LSTMcell,nunits hidden units each, for each layer
    	if nlayer>1:
    		cells=[]
    		for _ in range(nlayer):
    			cell = rnn.GRUCell(nunits)
    			cells.append(cell)
    		cell = tf.contrib.rnn.MultiRNNCell(cells)
    	else:
    		cell = rnn.GRUCell(nunits)
    	outputs,_=rnn.static_rnn(cell, x, dtype="float32")
    	outputs = tf.stack(outputs, axis=1)

    	# Trainable parameters
    	attention_size = int(nunits / 2)
    	W_omega = tf.Variable(tf.random_normal([nunits, attention_size], stddev=0.1))
    	b_omega = tf.Variable(tf.random_normal([attention_size], stddev=0.1))
    	u_omega = tf.Variable(tf.random_normal([attention_size], stddev=0.1))

    	# Applying fully connected layer with non-linear activation to each of the B*T timestamps;
    	#  the shape of `v` is (B,T,D)*(D,A)=(B,T,A), where A=attention_size
    	v = tf.tanh(tf.tensordot(outputs, W_omega, axes=1) + b_omega)
    	# For each of the timestamps its vector of size A from `v` is reduced with `u` vector
    	vu = tf.tensordot(v, u_omega, axes=1)   # (B,T) shape
    	alphas = tf.nn.softmax(vu)              # (B,T) shape also

    	output = tf.reduce_sum(outputs * tf.expand_dims(alphas, -1), 1)
    	output = tf.reshape(output, [-1, nunits])
    	return output



def getPrediction(x,nunits, nlayer, n_timetamps, tag):
    outw = tf.Variable(tf.random_normal([nunits,nclasses]),name='W')
    outb = tf.Variable(tf.random_normal([nclasses]),name='B')
    features = None
    if tag == 'RNN':
        features = Rnn(x, nunits, nlayer, n_timetamps)
    if tag == 'ATT':
        features = RnnAttention(x, nunits, nlayer, n_timetamps)
    prediction = tf.matmul(features,outw,name='prediction')+outb
    return prediction


def Rnn(x, nunits, nlayer, n_timetamps):
    #PLACEHOLDERS + WEIGHT & BIAS DEF


    #Processing input tensor
    x = tf.unstack(x,n_timetamps,1)
    #print train_x.shape
    #Specify network architecture

    #MORE THEN ONE LAYER: list of GRUcell,nunits hidden units each, for each layer
    if nlayer>1:
        cells=[]
        for _ in range(nlayer):
            cell = tf.contrib.rnn.GRUCell(nunits)
            cells.append(cell)
        cell = tf.contrib.rnn.MultiRNNCell(cells)

    #SIGNLE LAYER: single GRUCell, nunits hidden units each
    else:
        cell = tf.contrib.rnn.GRUCell(nunits)
    outputs,_=rnn.static_rnn(cell, x, dtype="float32")
    #CLASSIFICATION: Fully connected layer
    return outputs[-1]


def checkTest(data, batch_size, test_y, sess):

    accS=0
    tot_pred = []
    iterations = len(data) / batch_size
    if len(data) % batch_size != 0:
        iterations+=1

    cl = np.zeros(len(data))

    for ibatch in range(iterations):

        #BATCH_X BATCH_Y: i-th batches of test_x and test_y
        ba_x, batch_y = getBatch2(data, cl, ibatch, batch_size)

        #PRED_TEMP: (n,3)
        pred_temp = sess.run(prediction,feed_dict={x:ba_x})

        #TOT_PRED: accumulate max argument of each test prediciton evaluated by batch
        #TOT_PRED: (n,1)
        for el in pred_temp:
            tot_pred.append( np.argmax(el) )

    #GT: contains max argument of each test ground truth
    # test_y (n,3) -> gt (n,1)
    gt = []
    for el in test_y:
        gt.append( np.argmax(el))

    tot_pred = tot_pred[0:len(gt)]

    test_acc = accuracy_score(gt, tot_pred)
    print "TEST Accuracy ", test_acc


    #SAVE GROUD TRUTH gt E PREDICTION tot_pred
    var_totpred = './dataset/N%d/%s_%dl_truthpred_%dp%du%db/totpred%d.npy'%(norm,tag,nlayer,p_split,nunits,batchsz,itr)
    var_gt = './dataset/N%d/%s_%dl_truthpred_%dp%du%db/gt%d.npy'%(norm,tag,nlayer,p_split,nunits,batchsz,itr)
    np.save(var_totpred, tot_pred)
    np.save(var_gt, gt)

    return test_acc




def train_RNN(test_x,test_y,train_x,train_y,hm_epochs,itr,nlayer):

    '''

    '''


#Load dataset and get the number of elements in the label column
def getClasses(norm):

    aux='./dataset/N%d/ds.npy'%norm
    ds= np.load(aux)
    r_,c_= ds.shape

    #Label column
    aux=ds[:,286]

    aux=aux.astype(int)
    classes = np.argwhere(np.bincount(aux)).shape

    return classes[0]


print "GRU"

timesteps = 22
ninput = 13
p_split = 70
n_split=10

batchsz=int(sys.argv[1])
nunits=int(sys.argv[2])
hm_epochs=int(sys.argv[3])
nlayer=int(sys.argv[4])
norm=int(sys.argv[5])
itr =int(sys.argv[6])
tag = sys.argv[7]

nclasses=getClasses(norm)

print "Split percentage:\t",p_split
print "Number of split:\t",n_split
print "nclasses:",nclasses
print "batch size:",batchsz
print "n_units:",nunits
print "epoche:",hm_epochs
print "layers:",nlayer

#directory = './dataset/N%d/GRU_%dl_truthpred_%dp%du%db'%(norm,nlayer,p_split,nunits,batchsz)
directory = './dataset/N%d/%s_%dl_truthpred_%dp%du%db'%(norm,tag,nlayer,p_split,nunits,batchsz)
if not os.path.exists(directory):
    os.makedirs(directory)


#for i in range(n_split):
print "iter:",itr

var_train_x = './dataset/N%d/dati_m/TimeSeries/train_x%d_%d.npy'%(norm,itr,p_split)
var_train_y = './dataset/N%d/dati_m/ground_truth/train_y%d_%d.npy'%(norm,itr,p_split)
var_test_x = './dataset/N%d/dati_m/TimeSeries/test_x%d_%d.npy'%(norm,itr,p_split)
var_test_y = './dataset/N%d/dati_m/ground_truth/test_y%d_%d.npy'%(norm,itr,p_split)


train_x = np.load(var_train_x)
train_y = np.load(var_train_y)
test_x = np.load(var_test_x)
test_y = np.load(var_test_y)

#print np.unique(train_y)
#print np.unique(test_y)

#Start training
#train_RNN(test_x, test_y, train_x, train_y,hm_epochs,i,nlayer)

#DEF PLACEHOLDERS
x = tf.placeholder("float",[None,timesteps,ninput],name="x")
y = tf.placeholder("float",[None,nclasses],name="y")
learning_rate = tf.placeholder(tf.float32, shape=[])

sess = tf.InteractiveSession()

#Call RNN specifying number of layers
#prediction = Rnn(x,nlayer)
prediction = getPrediction(x,nunits, nlayer, timesteps, tag)

#LOSS-OPTIMIZER-ACCURACY DEF
tensor1d = tf.nn.softmax_cross_entropy_with_logits(labels=y,logits=prediction)
cost = tf.reduce_mean(tensor1d)

optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(cost)
#optimizer = tf.train.RMSPropOptimizer(learning_rate=learning_rate).minimize(cost)

correct = tf.equal(tf.argmax(prediction,1),tf.argmax(y,1))
accuracy = tf.reduce_mean(tf.cast(correct,tf.float64))

tf.global_variables_initializer().run()

#START SESSION
#(n,22*13) -> (n,22,13)
# (n,1) -> (n,checkTest3)
train_x = getRNNFormat(train_x)
train_y = getRNNFormatLabel(train_y)


test_x = getRNNFormat(test_x)
test_y = getRNNFormatLabel(test_y)


iterations = len(train_x) / batchsz

if len(train_x) % batchsz != 0:
    iterations+=1

#print "n iterations: %d" % iterations

#print train_y.shape
#print test_y.shape

best_test_acc=0.0

for e in range(hm_epochs):

    train_x, train_y = shuffle(train_x, train_y)


    lossi = 0
    accS = 0

    for ibatch in range(iterations):

        #BATCH_X BATCH_Y: i-th batches of train_x and train_y
        batch_x, batch_y = getBatch2(train_x, train_y, ibatch, batchsz)
        acc,_,loss = sess.run([accuracy,optimizer,cost],feed_dict={x:batch_x,y:batch_y, learning_rate:0.0001})
        #if ibatch % 50 == 0:
            #print "\t acc %f" % acc
        lossi+=loss
        accS+=acc

    print "Epoch ",e," Train loss:",lossi/iterations,"| accuracy:",accS/iterations
    test_acc = checkTest(test_x, batchsz, test_y, sess)

    if test_acc > best_test_acc:
        best_test_acc = test_acc
        print "BEST:", best_test_acc


path_save = directory+'/TESTAccuracy.txt'
f = open(path_save, 'a')
s = str(best_test_acc)
f.write(s+'\n')
f.close()
