import numpy as np
import tensorflow as tf
import pandas as ps

mnist = input_data.read_data_sets("/tmp/data/",one_hot=True)

def conv2d(x,W):
    return tf.nn.conv2d(x,W,strides=[1,1,1,1],padding='SAME')

def maxpool2d(x):
    return tf.nn.max_pool(x, ksize=[1,2,2,1],strides=[1,2,2,1],padding='SAME')

def cnn(x,hm_epochs,height,width,bands,batch_sz,n_classes):
    W = {
        #5x5 convolution 1 img input 32 output
        'W_conv1': tf.Variable(tf.random_normal([5,5,1,32]),name='WCI')
        #5x5 convo 32 input 64 output
        'W_conv2': tf.Variable(tf.random_normal([5,5,32,64]),name='WCII')
        #1024 inputs, n_classes outputs
        'W_out': tf.Variable(tf.random_normal([64,n_classes]),name="WOUT")
    }
    b = {
        #5x5 convolution 1 img input 32 output
        'b_conv1': tf.Variable(tf.random_normal([32]),name='BCI')
        #5x5 convo 32 input 64 output
        'b_conv2': tf.Variable(tf.random_normal([64]),name='BCII')
        #1024 inputs, n_classes outputs
        'b_out': tf.Variable(tf.random_normal([n_classes]),name="BOUT")
    }

    #reshape to 4D tensor
    x = tf.reshape(x,shape=[-1,height,width,1])


    conv1 = tf.nn.relu(conv2d(x,W['W_conv1'])+b['b_conv1'])
    conv1 = maxpool2d(conv1)

    conv2 = tf.nn.relu(conv2d(x,W['W_conv2'])+b['b_conv2'])
    conv2 = maxpool2d(conv2)

    features = agbpool(conv2)

    output = tf.matmul(features, W['W_out'],name="prediction")+b['b_out']

    return output, features

def train_nn(x,hm_epochs,height,width,bands,batch_sz,n_classes):
    sess = tf.InteractiveSession()

    prediction, feat = cnn(x,hm_epochs,height,width,bands,batch_sz,n_classes)
    tensor1d = tf.nn.softmax_cross_entropy_with_logits(labels=y, logits=prediction)
    cost = tf.reduce_mean(tensor1d)
    optimizer = tf.train.AdamOptimizer().minimize(cost)

    tf.global_variables_initializer().run()
    saver = tf.train.Saver()

    j = 0
    for e in range(hm_epochs):

        e_loss=0

        for _ in range( int(mnist.train.num_examples/batch_sz) ):
            batch_x, batch_y = mnist.train.next_batch(natch_sz)
            _,c = sess.run([optimizer,cost], feed_dict={x: batch_x, y:batch_y})

            e_loss += c
            j+=1
    correct = tf.equal(tf.argmax(prediction,1),tf.argmax(y,1))
    accuracy = tf.reduce_mean(tf.cast(correct,'float'))

    saver.save(sess,'./output/modelCNN')
    write.close()
    print "Accuracy:",accuracy.eval({x:mnist.test.images, y:mnist.test.labels})


ds_img = np.load('./dataset/N1/dsHR_img.npy')

hm_epochs=200
height = 28
width = 28
bands =
batch_sz = 128
n_classes = 10

x = tf.placeholder(tf.float32, [None, height*width ], name="x")
y = tf.placeholder(tf.float32, [None, n_classes], name="y")

train_nn(ds_img,hm_epochs,height,width,bands,batch_sz,n_classes)
