import sys
import os
import numpy as np
import math
from operator import itemgetter, attrgetter, methodcaller
import tensorflow as tf
from tensorflow.contrib import rnn
import random
from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_recall_fscore_support
from sklearn.metrics import f1_score
from sklearn.ensemble import RandomForestClassifier
from sklearn.utils import shuffle
from sklearn.metrics import confusion_matrix

def CNN3(x_inp, n_filters, dropout, is_training):

	scaling = 1

	conv0 = tf.layers.conv2d(inputs=x_inp, filters=64*scaling, padding='valid', kernel_size=[7,7], strides=(2,2), activation=tf.nn.relu)
	conv0 = tf.layers.batch_normalization(conv0)
	print "conv0.get_shape"
	print conv0.get_shape()

	conv1 = tf.layers.conv2d(inputs=conv0, filters=112*scaling, padding='valid', kernel_size=[4,4], activation=tf.nn.relu)
	conv1 = tf.layers.batch_normalization(conv1)
	print "conv1.get_shape"
	print conv1.get_shape()

	conv2 = tf.layers.conv2d(inputs=conv1, filters=80*scaling, padding='valid', kernel_size=[3,3], activation=tf.nn.relu)
	conv2 = tf.layers.batch_normalization(conv2)
	print "conv2.get_shape"
	print conv2.get_shape()

	conv3 = tf.layers.conv2d(inputs=conv2, filters=n_filters, padding='valid', kernel_size=[1,1], activation=tf.nn.relu)
	conv3 = tf.layers.batch_normalization(conv3)
	print "conv3.get_shape"
	print conv3.get_shape()

	c_shape = conv3.get_shape()

	cnn = tf.reduce_mean(conv3, [1,2])
	print cnn.get_shape()

	tensor_shape = cnn.get_shape()

	return cnn, tensor_shape[1].value

####################ResNet##############################

def ResBlock(x_inp, n_filters, stride):

	print "\tx_inp.get_shape: "
	print x_inp.get_shape()

	c1 = tf.layers.conv2d( x_inp, filters = n_filters, padding="same", kernel_size=[3,3], strides = (stride, stride), activation=tf.nn.relu)
	c1 = tf.layers.batch_normalization(c1)

	print "\tc1.get_shape: "
	print c1.get_shape()

	c2 = tf.layers.conv2d( c1, filters = n_filters, padding="same", kernel_size=[3,3], activation=tf.nn.relu )
	c2 = tf.layers.batch_normalization(c2)

	print "\tc2.get_shape: "
	print c2.get_shape()

	residual = x_inp

	if stride == 2:
		residual = tf.layers.conv2d( x_inp, filters = n_filters, padding="same", strides = (stride, stride), kernel_size=[1,1])

	summation = residual + c2

	summation = tf.nn.relu(summation)

	print "\tsummation.get_shape: "
	print summation.get_shape()
	print "======================"

	return summation


def ResNet( input_layer, n_filters):

	print"input_layer:", input_layer.get_shape()
	outCBP = ResNConvBnPool(input_layer,n_filters)

	n_blocks = 2
	n_repet = 1

	print "n_filter: %d"%n_filters
	print"outCBP:", outCBP.get_shape()

	outRes = outCBP
	current_n_filters = n_filters / 2
	stride_val = 1
	for nb in range(n_blocks):
		for nr in range(n_repet):
			outRes = ResBlock(outRes, current_n_filters, stride_val)
			stride_val = 1
		stride_val = 2
		current_n_filters = current_n_filters * 2

	fin_conv = tf.layers.conv2d( outRes, filters = nunits, kernel_size=[1,1], activation = tf.nn.relu )

	bn = tf.layers.batch_normalization(fin_conv)

	features = tf.reduce_mean(bn, [1,2])
	print features.get_shape()

	tensor_shape = features.get_shape()
	features = tf.nn.tanh(features)

	return features, tensor_shape[1].value


def ResNConvBnPool(x,n_filter):

	conv = tf.layers.conv2d( x,filters= n_filter/2, kernel_size=[7, 7], padding="valid", activation=tf.nn.relu)
	bn = tf.layers.batch_normalization(conv)

	return bn

def ResNBlockEval( x, n_filter, n_stride, itr ):
	conv0 = tf.layers.conv2d( x,filters=n_filter, kernel_size=[1, 1], padding="valid")
	bn0 = tf.layers.batch_normalization( conv0 )
	relu0 = tf.nn.relu( bn0 )

	conv1 = tf.layers.conv2d( relu0, filters=n_filter, kernel_size=[3, 3], padding="same")
	bn1 = tf.layers.batch_normalization( conv1 )
	relu1 = tf.nn.relu( bn1 )


	conv2 = tf.layers.conv2d( relu1, filters=n_filter, kernel_size=[3, 3], padding="same")
	bn2 = tf.layers.batch_normalization( conv2 )

	summ = tf.add( conv0, bn2 )
	relu2 = tf.nn.relu( summ )

	print"\n=======Block_%d_filter(%d)==========="%(itr,n_filter)
	print"input: ",x.get_shape()
	print "conv1x1_bn_relu: ",relu0.get_shape()
	print"conv3x3_bn_relu: ",relu1.get_shape()
	print"conv3x3_bn: ",bn2.get_shape()
	print"summ: ",summ.get_shape()
	print"relu: ",relu2.get_shape()
	#print"drop_out: ",drop_out.get_shape()
	print"================================="
	print"================================="

	return relu2

####################ResNet##############################


####################Simple Dense Net##############################
def ConvBnPool(x):
	conv7 = tf.layers.conv2d(x,
                            filters = 256,
                            kernel_size = [ 7, 7 ],
                            strides = ( 2, 2 ),
                            padding = "valid",
                            activation = tf.nn.relu)

	conv7 = tf.layers.batch_normalization(conv7)
	conv7 = tf.layers.max_pooling2d( conv7, pool_size = [3, 3], strides=2)
	return conv7


def DenseBasicBlock( inp, drop_rate, is_training, reduction_layer, feat_layer ):
	conv1 = tf.layers.batch_normalization(inp)
	conv1 = tf.nn.relu(conv1)
	conv1 = tf.layers.conv2d( conv1, filters=reduction_layer, kernel_size=[1, 1], padding="valid")
	conv1 = tf.layers.dropout( conv1, rate=drop_rate, training=is_training)

	conv3 = tf.layers.batch_normalization(conv1)
	conv3 = tf.nn.relu(conv3)
	conv3 = tf.layers.conv2d( conv3, filters=feat_layer, kernel_size=[3, 3], padding="same")
	conv3 = tf.layers.dropout( conv3, rate=drop_rate, training=is_training)

	return conv3


def DenseBlock(inp, prev_out, drop_rate, is_training, reduction_layer, feat_layer):
	block = inp
	if len(prev_out) !=0:
		for i in range(len(prev_out)):
			block = tf.concat([block, prev_out[i]], 3)
	print "\tblock ", block.get_shape()
	out_block = DenseBasicBlock( block, drop_rate, is_training, reduction_layer, feat_layer)
	return out_block

def DenseNet(x_cnn, nunits, drop_rate, is_training):

	reduction_layer = 64
	feat_layer = 128
	n_blocks = 6
	firstCBP = ConvBnPool(x_cnn)

	f_block = DenseBlock(firstCBP, [], drop_rate, is_training, reduction_layer, feat_layer)
	prev_blocks = []
	last_block = prev_blocks
	for i in range(n_blocks-1):
		last_block = DenseBlock(f_block, prev_blocks, drop_rate, is_training, reduction_layer, feat_layer)
		prev_blocks.append( last_block )
		print "last_block.get_shape"
		print last_block.get_shape()

	fin_conv = tf.layers.batch_normalization(last_block)

	fin_conv = tf.nn.relu(fin_conv)
	fin_conv = tf.layers.conv2d( last_block, filters = nunits, kernel_size=[1,1])

	print "fin_conv.get_shape()"
	print fin_conv.get_shape()

	features = tf.reduce_max(fin_conv, [1,2])

	print "features.get_shape()"
	print features.get_shape()

	tensor_shape = features.get_shape()
	features = tf.nn.tanh(features)

	return features, tensor_shape[1].value

###################################################################

def extractFeatures(ts_data, vhsr_data):
	batchsz = 1024
	iterations = ts_data.shape[0] / batchsz

	if ts_data.shape[0] % batchsz != 0:
	    iterations+=1


	label_test = np.zeros(len(ts_data))
	test_y = np.zeros(len(ts_data))

	features = None

	for ibatch in range(iterations):

		ts_batch_x, batch_y = getBatch(ts_data, label_test, ibatch, batchsz)
		bach_rnn_x_b = ts_batch_x[::-1]
		vhs_batch_x, batch_y = getBatch(vhsr_data, test_y, ibatch, batchsz)

		partial_features = sess.run(features_learnt,feed_dict={x_rnn:ts_batch_x,
																#x_rnn_b:bach_rnn_x_b,
																x_cnn:vhs_batch_x})
		if features is None:
			features = partial_features
		else:
			features = np.vstack((features, partial_features))

	print features.shape
	return features
'''
def checkTest(ts_data, vhsr_data, batchsz, label_test):

    tot_pred = []

    iterations = ts_data.shape[0] / batchsz

	if ts_data.shape[0] % batchsz != 0:
	    iterations+=1

	for ibatch in range(iterations):

        ts_batch_x, _ = getBatch(ts_data, label_test, ibatch, batchsz)
		vhs_batch_x, batch_y = getBatch(vhsr_data, test_y, ibatch, batchsz)

		pred_temp = sess.run(testPrediction,feed_dict={x_rnn:ts_batch_x,
														 is_training_ph:False,
														 dropout:0.0,
		 												 #x_rnn_b:ts_batch_x_b,
														 x_cnn:vhs_batch_x})

		del ts_batch_x
		del vhs_batch_x
		del batch_y

		for el in pred_temp:
			tot_pred.append( el )

	print np.bincount(np.array(tot_pred))
	print np.bincount(np.array(label_test))


	print "TEST F-Measure: %f" % f1_score(label_test, tot_pred, average='weighted')
	print f1_score(label_test, tot_pred, average=None)
	print "TEST Accuracy: %f" % accuracy_score(label_test, tot_pred)
	sys.stdout.flush()

	return accuracy_score(label_test, tot_pred)

'''

def RnnAttention(x, nunits, nlayer, n_timetamps, dropout):

	x = tf.unstack(x,n_timetamps,1)

	#NETWORK DEF
	#MORE THEN ONE LAYER: list of LSTMcell,nunits hidden units each, for each layer
	if nlayer>1:
		cells=[]
		for _ in range(nlayer):
			cell = rnn.GRUCell(nunits)
			cell = tf.nn.rnn_cell.DropoutWrapper(cell, output_keep_prob=dropout)

			cells.append(cell)
		cell = tf.contrib.rnn.MultiRNNCell(cells)
    #SIGNLE LAYER: single GRUCell, nunits hidden units each
	else:
		cell = rnn.GRUCell(nunits)
		cell = tf.nn.rnn_cell.DropoutWrapper(cell, output_keep_prob=dropout)

	outputs,_=rnn.static_rnn(cell, x, dtype="float32")
	outputs = tf.stack(outputs, axis=1)

	# Trainable parameters
	attention_size = nunits #int(nunits / 2)
	W_omega = tf.Variable(tf.random_normal([nunits, attention_size], stddev=0.1))
	b_omega = tf.Variable(tf.random_normal([attention_size], stddev=0.1))
	u_omega = tf.Variable(tf.random_normal([attention_size], stddev=0.1))

	v = tf.tanh(tf.tensordot(outputs, W_omega, axes=1) + b_omega)

	vu = tf.tensordot(v, u_omega, axes=1)   # (B,T) shape
	alphas = tf.nn.softmax(vu)              # (B,T) shape also

	output = tf.reduce_sum(outputs * tf.expand_dims(alphas, -1), 1)
	output = tf.reshape(output, [-1, nunits])

	return output


def CNN2(x, nunits):
	conv0 = tf.layers.conv2d(inputs=x, filters=64, kernel_size=[7, 7], padding="valid", activation=tf.nn.relu)

	conv0 = tf.layers.batch_normalization(conv0)
	print conv0.get_shape()
	pool0 = tf.layers.max_pooling2d(inputs=conv0, pool_size=[2, 2], strides=2)

	conv1 = tf.layers.conv2d( inputs=pool0, filters=nunits, kernel_size=[3, 3], padding="valid", activation=tf.nn.relu)
	conv1 = tf.layers.batch_normalization(conv1)
	print conv1.get_shape()

	conv2 = tf.layers.conv2d( inputs=conv1, filters=nunits, kernel_size=[3, 3], padding="valid", activation=tf.nn.relu)
	conv2 = tf.layers.batch_normalization(conv2)

	conv3 = tf.layers.conv2d( inputs=conv2, filters=nunits, kernel_size=[3, 3], padding="valid", activation=tf.nn.relu)
	conv3 = tf.layers.batch_normalization(conv3)
	print conv3.get_shape()

	conv3 = tf.layers.conv2d( inputs=conv3,
                                filters=nunits,
                                kernel_size=[1, 1],
                                padding="valid",
                                activation=tf.nn.relu)

	conv3 = tf.layers.batch_normalization(conv3)
	print conv3.get_shape()

	conv3_shape = conv3.get_shape()

	cnn = tf.reduce_mean(conv3, [1,2])
	print cnn.get_shape()

	tensor_shape = cnn.get_shape()

	return cnn, tensor_shape[1].value

def CNN(x, nunits):

    conv1 = tf.layers.conv2d( inputs=x,
                            filters=nunits/2, #256
                            kernel_size=[7, 7],
                            padding="valid",
                            activation=tf.nn.relu)
    conv1 = tf.layers.batch_normalization(conv1)
    print conv1.get_shape()
    pool1 = tf.layers.max_pooling2d(inputs=conv1, pool_size=[2, 2], strides=2)
    conv2 = tf.layers.conv2d( inputs=pool1,
                            filters=nunits,
                            kernel_size=[3, 3],
                            padding="valid",
                            activation=tf.nn.relu)
    conv2 = tf.layers.batch_normalization(conv2)
    print conv2.get_shape()
    conv3 = tf.layers.conv2d( inputs=conv2,
                            filters=nunits,
                            kernel_size=[3, 3],
                            padding="same",
                            activation=tf.nn.relu)
    conv3 = tf.layers.batch_normalization(conv3)
    conv3 = tf.concat([conv2,conv3],3)
    print conv3.get_shape()
    conv4 = tf.layers.conv2d( inputs=conv3,
                            filters=nunits,
                            kernel_size=[1, 1],
                            padding="valid",
                            activation=tf.nn.relu)
    conv4 = tf.layers.batch_normalization(conv4)
    print conv4.get_shape()
    conv4_shape = conv4.get_shape()

    cnn = tf.reduce_mean(conv4, [1,2], name="features_cnn")
    print cnn.get_shape()
    tensor_shape = cnn.get_shape()

    return cnn, tensor_shape[1].value



def BiRNN(x, nunits, nlayer, n_timetamps):

    x = tf.unstack(x,n_timetamps,1)

    b_cell = None
    f_cell = None

    if nlayer>1:
        f_cells=[]
        b_cells=[]

        for _ in range(nlayer):
            f_cells.append( rnn.GRUCell(nunits) )
            b_cells.append( rnn.GRUCell(nunits) )

        b_cell = tf.contrib.rnn.MultiRNNCell(b_cells)
        f_cell = tf.contrib.rnn.MultiRNNCell(f_cells)

    else:
        b_cell = rnn.GRUCell(nunits)
        f_cell = rnn.GRUCell(nunits)

    outputs,_,_=rnn.static_bidirectional_rnn(f_cell, b_cell, x, dtype=tf.float32)

    return outputs[-1]


def Rnn(x, nunits, nlayer, n_timetamps):

    x = tf.unstack(x,n_timetamps,1)

    if nlayer>1:
        cells=[]

        for _ in range(nlayer):
            cell = rnn.GRUCell(nunits)
            cells.append(cell)
        cell = tf.contrib.rnn.MultiRNNCell(cells)

    else:
        cell = rnn.GRUCell(nunits)

    outputs,_=rnn.static_rnn(cell, x, dtype="float32")

    return outputs[-1]

def RnnFull(x, nunits, nlayer):

    n_timetamps = 22
    x = tf.unstack(x,n_timetamps,1)

    if nlayer>1:
        cells=[]
        for _ in range(nlayer):
            cell = rnn.LSTMCell(nunits)
            cells.append(cell)
        cell = tf.contrib.rnn.MultiRNNCell(cells)
    else:
        cell = rnn.LSTMCell(nunits)

    outputs,_=rnn.static_rnn(cell, x, dtype="float32")
    features = tf.reshape(outputs, [-1, nunits*n_timetamps])

    print features.get_shape()

    return features

def getBatch(X, Y, i, batch_size):

    start_id = i*batch_size
    end_id = min( (i+1) * batch_size, X.shape[0])
    batch_x = X[start_id:end_id]
    batch_y = Y[start_id:end_id]

    return batch_x, batch_y

def getLabelFormat(Y):

    vals = np.unique( Y )
    sorted(vals)
    hash_val = {}

    for el in vals:
        hash_val[el] = len(hash_val.keys())

    new_Y = []

    for el in Y:
        t = np.zeros(len(vals))
        t[hash_val[el]] = 1.0
        new_Y.append(t)

    return np.array(new_Y)

def getRNNFormat(X):

    new_X = []
    for row in X:
        new_X.append( np.split(row, 34) )

    return np.array(new_X)


#def getPrediction(x_rnn, x_rnn_b, x_cnn, nunits, nlayer, nclasses, choice):
def getPrediction(x_rnn, x_cnn, nunits, nlayer, nclasses, dropout, is_training):

    n_timetamps = 34
    features_learnt = None

    prediction = None
    features_learnt = None

    vec_rnn = RnnAttention(x_rnn, nunits, nlayer, n_timetamps, dropout)

    vec_cnn, cnn_dim = CNN(x_cnn, 512)

    features_learnt=tf.concat([vec_rnn,vec_cnn],axis=1, name="features")
    first_dim = cnn_dim + nunits

    #Classifier1 #RNN Branch
    print "RNN Features:"
    print vec_rnn.get_shape()
    outb1 = tf.Variable(tf.truncated_normal([nclasses]),name='B1')
    outw1 = tf.Variable(tf.truncated_normal([nunits,nclasses]),name='W1')
    pred_c1 = tf.matmul(vec_rnn,outw1)
    pred_c1 = tf.add(pred_c1,outb1,name="prediction_rnn")

    #Classifier2 #CNN Branch
    print "CNN Features:"
    print vec_cnn.get_shape()
    outb2 = tf.Variable(tf.truncated_normal([nclasses]),name='B2')
    outw2 = tf.Variable(tf.truncated_normal([cnn_dim,nclasses]),name='W2')
    pred_c2 = tf.matmul(vec_cnn,outw2)
    pred_c2 = tf.add(pred_c2,outb2,name="prediction_cnn")

    #ClassifierFull
    print "FULL features_learnt:"
    print features_learnt.get_shape()
    outb = tf.Variable(tf.truncated_normal([nclasses]),name='B')
    outw = tf.Variable(tf.truncated_normal([first_dim,nclasses]),name='W')
    pred_full = tf.matmul(features_learnt,outw)
    pred_full = tf.add(pred_full,outb,name="prediction_full")

    return pred_c1, pred_c2, pred_full, features_learnt


def data_augmentation( label_train, ts_train, vhs_train ):

    new_label_train = []
    new_ts_train = []
    new_vhs_train = []

    for i in range(vhs_train.shape[0]):
        img = vhs_train[i]

        #ROTATE
        for j in range(1,5):
            img_rotate = np.rot90(img, k=j, axes=(0, 1))
            new_label_train.append( label_train[i] )
            new_ts_train.append( ts_train[i])
            new_vhs_train.append( img_rotate )

        #FLIPPING
        for j in range(2):
            img_flip = np.rot90(img, j)
            new_label_train.append( label_train[i] )
            new_ts_train.append( ts_train[i])
            new_vhs_train.append( img_flip )

        #TRANSPOSE
        t_img = np.transpose(img, (1,0,2))
        new_label_train.append( label_train[i] )
        new_ts_train.append( ts_train[i])
        new_vhs_train.append( t_img )

    return np.array(new_label_train), np.array(new_ts_train), np.array(new_vhs_train)




#Model parameters
nunits = 1024
batchsz = 64
hm_epochs = 400
n_levels_lstm = 1
droprate_rnn = 0.6

#Data INformation
n_timestamps = 34
n_dims = 16
patch_window = 25
n_channels = 5
p_split=30

itr = int(sys.argv[1])
path_in_vhsr = sys.argv[2]
path_in_gt = sys.argv[3]
path_in_ts = sys.argv[4]

train_x_vhs = np.load( path_in_vhsr+"train_x%d_%d.npy"%( itr, p_split ) )
train_x_ts = np.load( path_in_ts+"train_x%d_%d.npy"%( itr, p_split ) )
train_y = np.load( path_in_gt+"train_y%d_%d.npy"%( itr, p_split ) )

nclasses = np.bincount( train_y ).shape[0]-1

train_y = getLabelFormat( train_y )
train_x_ts = getRNNFormat( train_x_ts )

height = train_x_vhs.shape[1]
width = train_x_vhs.shape[2]
if height!=width:
    train_x_vhs = np.swapaxes(train_x_vhs, 1, 3)
    tag_band = '4bands'
else:
    tag_band = 'NDVI'


print "n_bands",tag_band
height = train_x_vhs.shape[1]
width = height
band = train_x_vhs.shape[3]

print "n_classes",nclasses

directory = './dataset/m3fusion_0.6DO_%d_%d_%s/'%( p_split, batchsz, tag_band)
if not os.path.exists(directory):
    os.makedirs(directory)


path_out_model = directory+'modelTT%d/'%itr
if not os.path.exists(path_out_model):
    os.makedirs(path_out_model)


flog = open(directory+"log.txt","a")


x_rnn = tf.placeholder("float",[None,n_timestamps,n_dims],name="x_rnn")
x_cnn = tf.placeholder("float",[None,patch_window,patch_window,n_channels],name="x_cnn")
y = tf.placeholder("float",[None,nclasses],name="y")


learning_rate = tf.placeholder(tf.float32, shape=[], name="learning_rate")
is_training_ph = tf.placeholder(tf.bool, shape=(), name="is_training")
dropout = tf.placeholder(tf.float32, shape=(), name="drop_rate_rnn")


sess = tf.InteractiveSession()


pred_c1, pred_c2, pred_full, features_learnt = getPrediction(x_rnn, x_cnn, nunits, n_levels_lstm, nclasses, dropout, is_training_ph)
testPrediction = tf.argmax(pred_full, 1, name="prediction")


loss_full = tf.reduce_mean( tf.nn.softmax_cross_entropy_with_logits(labels=y,logits=pred_full)  )
loss_c1 = tf.reduce_mean( tf.nn.softmax_cross_entropy_with_logits(labels=y,logits=pred_c1)  )
loss_c2 = tf.reduce_mean( tf.nn.softmax_cross_entropy_with_logits(labels=y,logits=pred_c2)  )


cost = loss_full + (0.3 * loss_c1) + (0.3 * loss_c2)


optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(cost)


correct = tf.equal(tf.argmax(pred_full,1),tf.argmax(y,1))
accuracy = tf.reduce_mean(tf.cast(correct,tf.float64))


#to have tensorboard
tf.summary.scalar("cost function", cost)
writer = tf.summary.FileWriter(path_out_model+"histogram_example")
summaries = tf.summary.merge_all()


tf.global_variables_initializer().run()


# Add ops to save and restore all the variables.
saver = tf.train.Saver()


iterations = train_x_ts.shape[0] / batchsz


if train_x_ts.shape[0] % batchsz != 0:
    iterations+=1


best_loss = sys.float_info.max
j = 0

for e in range(hm_epochs):

    train_x_ts, train_x_vhs, train_y = shuffle(train_x_ts, train_x_vhs, train_y, random_state=0)
    lossi = 0
    accS = 0

    for ibatch in range(iterations):

        ts_batch_x, _ = getBatch(train_x_ts, train_y, ibatch, batchsz)
        vhs_batch_x, batch_y = getBatch(train_x_vhs, train_y, ibatch, batchsz)

        summ, acc, _, loss = sess.run( [ summaries, accuracy, optimizer, cost ], feed_dict = { x_rnn:ts_batch_x,
                                                                                                x_cnn:vhs_batch_x,
                                                                                                y:batch_y,
                                                                                                is_training_ph:True,
                                                                                                dropout:0.6,
                                                                                                learning_rate:0.0002} )

        accS+=acc
        lossi+=loss
        writer.add_summary( summ, j )
        j += 1

        del ts_batch_x
        del vhs_batch_x
        del batch_y

    loss_epoch = float(lossi/iterations)
    acc_epoch = float(accS/iterations)
    print "Epoch: %d Train loss: %f| accuracy: %f"%( e, loss_epoch, acc_epoch )
    flog.write( "Epoch: %d Train loss: %f| accuracy: %f"%( e, loss_epoch, acc_epoch ) )

    if loss_epoch < best_loss:

        best_loss = loss_epoch
        save_path = saver.save(sess, path_out_model+'model', global_step = itr)

flog.close()

'''
if e % 40 == 0:
    train_features = extractFeatures(ts_train, vhs_train)
    label_features = []
    for el in train_y:
        label_features.append( np.argmax(el) )
    clf = RandomForestClassifier(n_estimators = 200)
    clf.fit(train_features, label_features)
    test_features = extractFeatures(rnn_data_test, vhsr_test)
    rf_classif = clf.predict(test_features)
    print "RF(NEURAL) TEST F-Measure: %f" % f1_score(classes_test, rf_classif, average='weighted')
    print f1_score(classes_test, rf_classif, average=None)
    print "RF(NEURAL) TEST Accuracy: %f" % accuracy_score(classes_test, rf_classif)
    print "============================================================"
'''
