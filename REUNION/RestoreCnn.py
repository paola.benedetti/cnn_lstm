import sys
import os
import tensorflow as tf
import numpy as np
from sklearn.metrics import accuracy_score, cohen_kappa_score, f1_score
from sklearn.metrics import precision_recall_fscore_support
from sklearn.ensemble import RandomForestClassifier


#Format values matrix from (n,22*13) shape to (n,22,13)
def getRNNFormat(X):

    #print X.shape
    new_X = []
    for row in X:
        new_X.append( np.split(row, 22) )
    return np.array(new_X)

#Format labels matrix from (n,1) shape to (n,3)
def getRNNFormatLabel(Y):

    vals = np.unique(np.array(Y))
    sorted(vals)
    hash_val = {}

    for el in vals:
        hash_val[el] = len(hash_val.keys())

    new_Y = []

    for el in Y:
        t = np.zeros(len(vals))
        t[hash_val[el]] = 1.0
        new_Y.append(t)

    return np.array(new_Y)

#Get i-th batches of values set X and labels set Y
def getBatch(X, Y, i, batch_size):
    start_id = i*batch_size
    end_id = min( (i+1) * batch_size, X.shape[0])

    batch_x = X[start_id:end_id]
    batch_y = Y[start_id:end_id]

    return batch_x, batch_y

def getLabelFormat(Y):
	vals = np.unique(np.array(Y))
	sorted(vals)
	hash_val = {}
	for el in vals:
		hash_val[el] = len(hash_val.keys())
	new_Y = []
	for el in Y:
		t = np.zeros(len(vals))
		t[hash_val[el]] = 1.0
		new_Y.append(t)
	return np.array(new_Y)


#MAIN
itr = int( sys.argv[1] )
p_split = 100*float( sys.argv[2] )
batch_size = int( sys.argv[3] )
name = str( sys.argv[4] )
path_in_val = sys.argv[5]
path_in_gt = sys.argv[6]
g_path = sys.argv[7]

#declare paths
#g_path = "./dataset/NDVI/15x15/"
net_path = g_path+"%s%d_%d/"%(name, p_split, batch_size)
path_in_dir = g_path+'%s%d_%d/modelTT%d/'%( name, p_split, batch_size,itr ) #model path
path_in_model = path_in_dir+'model-%d.meta'%itr #model file
#path_in_x = path_in_val+'test_x%d_%d.npy'%(itr,p_split) #values
#path_in_y = path_in_gt+'test_y%d_%d.npy'%(itr,p_split) #gt

ftest_acc = open(net_path+"test_accuracy.txt", "a")
ftest_fscore = open(net_path+"test_fscore.txt","a")
ftest_precisions = open(net_path+"test_prec_classes.txt","a")
ftest_recall = open(net_path+"test_rec_classes.txt","a")
ftest_scores = open(net_path+"test_fscore_classes.txt","a")

frfc_acc = open(net_path+"RFCfeatures_accuracy.txt", "a")
frfc_fscore = open(net_path+"RFCfeatures_fscore.txt","a")
frfc_precisions = open(net_path+"RFCfeatures_prec_classes.txt","a")
frfc_recall = open(net_path+"RFCfeatures_rec_classes.txt","a")
frfc_scores = open(net_path+"RFCfeatures_fscore_classes.txt","a")


tf.reset_default_graph()

with tf.Session() as sess:

    model_saver = tf.train.import_meta_graph(path_in_model)
    model_saver.restore(sess, tf.train.latest_checkpoint(path_in_dir))
    print "Model Restored"

    writer = tf.summary.FileWriter( path_in_dir+'histogram_example', sess.graph )
    print "Graph Wrote"
    writer.close()

    graph = tf.get_default_graph()

    x = graph.get_tensor_by_name("x:0")
    y = graph.get_tensor_by_name("y:0")
    #in densenet
    f = graph.get_tensor_by_name("features:0")
    p = graph.get_tensor_by_name("prediction:0")



    #test_x = np.load(path_in_x)
    #test_y = np.load(path_in_y)

    #test_x = np.swapaxes(test_x, 1, 3)
    #test_y = getLabelFormat(test_y)

    #path to ds
    var_train_x = path_in_val+'train_x%d_%d.npy'%(itr,p_split)
    var_train_y = path_in_gt+'train_y%d_%d.npy'%(itr,p_split)
    var_test_x = path_in_val+'test_x%d_%d.npy'%(itr,p_split)
    var_test_y = path_in_gt+'test_y%d_%d.npy'%(itr,p_split)

    #load ds
    train_x = np.load(var_train_x)
    train_y = np.load(var_train_y)
    test_x = np.load(var_test_x)
    test_y = np.load(var_test_y)

    if train_x.shape[1]!=train_x.shape[2]:
        #(90857,65,65,4)
        train_x = np.swapaxes( train_x, 1, 3 )
        test_x = np.swapaxes( test_x, 1, 3 )

    #format data
    train_y = getLabelFormat( train_y )
    test_y = getLabelFormat( test_y )

    #EVAL TEST
    testAcc = 0
    testFscore = 0
    testFArray = np.array([])
    pred_test = []
    feat_test = []

    iter_test = len( test_x )/batch_size

    if len( test_y )%batch_size != 0:
        iter_test+=1

    cl = np.zeros( len( test_y ) )

    for i_batch in range( iter_test ):

        batch_x, batch_y = getBatch( test_x, cl, i_batch, batch_size )
        feat_temp, pred_temp = sess.run( [ f, p ], feed_dict = { x:batch_x} )#, tf_is_training_pl:False  in dense net

        for el in pred_temp:
            pred_test.append( np.argmax(el) )
        for el1 in feat_temp:
            feat_test.append(el1)

    #grounf_truth test
    gt_test = []
    for el in test_y:
        gt_test.append( np.argmax( el ) )

    pred_test = pred_test[ 0:len( gt_test )]

    testAcc = accuracy_score( gt_test, pred_test )
    testFscore = f1_score( gt_test, pred_test, average='weighted' )
    var_prec, var_rec, var_fsc, _ = precision_recall_fscore_support( gt_test, pred_test )

    print "Test Accuracy: %f\nTest Fscore:%f \nTest Fxclassses: %s\nTest Pxclasses:%s\nTest Rxclasses:%s\n" %( testAcc, testFscore, var_fsc, var_prec, var_rec )
    ftest_acc.write("%f\n"%testAcc)
    ftest_fscore.write("%f\n"%testFscore)
    for el in var_rec:
        ftest_recall.write("%f\t"%el)
    for el1 in var_prec:
        ftest_precisions.write("%f\t"%el1)
    for el2 in var_fsc:
        ftest_scores.write("%f\t"%el2)
    ftest_recall.write("\n")
    ftest_precisions.write("\n")
    ftest_scores.write("\n")

    #ftest.write("%f\n%f\n%s\n%s\n%s\n"%( testAcc, testFscore, var_fsc, var_prec, var_rec ))
    ftest_acc.close()
    ftest_fscore.close()
    ftest_recall.close()
    ftest_precisions.close()
    ftest_fscore.close()

    #EVAL RFC(features)
    pred_train = []
    feat_train = []
    iter_train = len( train_x )/batch_size

    if len( train_y )%batch_size != 0:
        iter_train += 1

    cl = np.zeros( len( train_y ) )

    for i_batch in range( iter_train ):

        batch_x, batch_y = getBatch( train_x, cl, i_batch, batch_size )
        feat_temp, pred_temp = sess.run( [ f, p ], feed_dict = { x:batch_x } )#, tf_is_training_pl:False in densenet

        for el in pred_temp:
            pred_train.append( np.argmax(el) )
        for el1 in feat_temp:
            feat_train.append(el1)


    clf = RandomForestClassifier( n_estimators = 200 )
    clf.fit( feat_train, pred_train )
    predC = clf.predict( feat_test )

    #rfv(features) vs ground truth
    RFCacc = accuracy_score( gt_test, predC )
    RFCfscore = f1_score( gt_test, predC, average='weighted' )
    RFCvar_prec, RFCvar_rec, RFCvar_fsc, _ = precision_recall_fscore_support( gt_test, predC )

    print "RFC Accuracy: %f\tRFC Fscore:%f \nRFC Rxclassses: %s\nRFC Fxclasses:%s\nRFC Pxclasses:%s\n" %( RFCacc, RFCfscore, str(RFCvar_rec), str(RFCvar_fsc), str(RFCvar_prec) )
    frfc_acc.write("%f\n"%RFCacc)
    frfc_fscore.write("%f\n"%RFCfscore)
    for el in RFCvar_rec:
        frfc_recall.write("%f\t"%el)
    for el1 in RFCvar_prec:
        frfc_precisions.write("%f\t"%el1)
    for el2 in RFCvar_fsc:
        frfc_scores.write("%f\t"%el2)

    frfc_recall.write("\n")
    frfc_precisions.write("\n")
    frfc_scores.write("\n")
    #ftest.write("%f\n%f\n%s\n%s\n%s\n"%( testAcc, testFscore, var_fsc, var_prec, var_rec ))
    frfc_acc.close()
    frfc_fscore.close()
    frfc_recall.close()
    frfc_precisions.close()
    frfc_fscore.close()
