import numpy as np
f = open('logStat.txt','w')
ds_label = np.load('dataset/ds_label.npy')
print "min class val:%d"%np.amin(ds_label[:,2])
print "max class val:%d"%np.amax(ds_label[:,2])
print "min obj val:%d"%np.amin(ds_label[:,3])
print "max obj val:%d"%np.amax(ds_label[:,3])
bin_count = np.bincount(ds_label[:,2])
obj_count = {}
avg_pixel = {}
f.write("%s\t%s\t%s\t%s\n"%("classe","n_pixel","n_objects","avg_pixelXobj"))
for i in range(1,bin_count.shape[0]):

    #obj occurrences per class
    tmp = np.bincount(ds_label[:,3][ds_label[:,2]==i])
    #total number of object per class
    obj_count[i] = np.sum(np.bincount(tmp[tmp!=0]))
    #number of pixel per class diveded by the number of object per class
    avg_pixel[i] = float(bin_count[i]/obj_count[i])
    print "%d/%d=%f"%(bin_count[i],obj_count[i],avg_pixel[i])
    f.write("%d\t%d\t%d\t%f\n"%(i,bin_count[i],obj_count[i],avg_pixel[i]))


print "\tbincount\n",bin_count
print "\tobjcount\n",obj_count
print "\tavgpixel\n",avg_pixel
f.close()
