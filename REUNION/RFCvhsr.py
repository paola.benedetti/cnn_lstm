from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.metrics import accuracy_score, cohen_kappa_score, f1_score
from sklearn.metrics import precision_recall_fscore_support
import os
import numpy as np
import sys

#python CLF RFC,SVC,GBC 1
itr = int(sys.argv[1])
p_split = float(sys.argv[2])
estimators = int(sys.argv[3])
path_in_val = sys.argv[4]
path_in_gt = sys.argv[5]

var_train_x = path_in_val+'train_x%d_%d.npy'%(itr,int(p_split*100))
var_train_y = path_in_gt+'train_y%d_%d.npy'%(itr,int(p_split*100))
var_test_x = path_in_val+'test_x%d_%d.npy'%(itr,int(p_split*100))
var_test_y = path_in_gt+'test_y%d_%d.npy'%(itr,int(p_split*100))

train_x = np.load(var_train_x)
train_y = np.load(var_train_y)
test_x = np.load(var_test_x)
test_y = np.load(var_test_y)

height = train_x.shape[1]
width = height
if train_x.shape[3]==5:
    n_bands = 'NDVI_'
else:
    n_bands = '_'

g_path = './dataset/NDVI/%dx%d/'%(height, width)

directory = g_path+'RFC%s/'%n_bands
if not os.path.exists(directory):
    os.makedirs(directory)

rfc_acc = open( directory+'rfc_accuracy.txt', 'a' )
rfc_fscore = open( directory+'rfc_fscores.txt', 'a' )
rfc_prec = open( directory+'rfc_precision.txt', 'a' )
rfc_recall = open( directory+'rfc_recall.txt', 'a' )
rfc_scores = open( directory+'rfc_scores.txt', 'a')


#random forest process 2D arrays
nsamples, nx, ny, band = train_x.shape
train_x = train_x.reshape((nsamples,nx*ny*band))

nsamples, nx, ny, band = test_x.shape
test_x = test_x.reshape((nsamples,nx*ny*band))

#print "train shape:", train_x.shape
#print "test shape: ",test_x.shape

#TRAIN WITH RANDOM FOREST
clf = RandomForestClassifier(n_estimators=estimators)
clf.fit(train_x,train_y)
predC = clf.predict(test_x)

#print "prediction shape", predC.shape

KAPPA = cohen_kappa_score( test_y, predC )
accuracy = accuracy_score( test_y, predC )
fscore = f1_score( test_y, predC, average='weighted' )
var_prec, var_rec, var_fsc, _ = precision_recall_fscore_support( test_y, predC )


print "\tSPLIT %d\nAccuracy: %f\nf-score: %f\nkappa: %f\n"%(itr, accuracy, fscore, KAPPA)

rfc_acc.write("%f\n"%accuracy)
rfc_fscore.write("%f\n"%fscore)
for el in var_prec:
    rfc_prec.write("%f\t"%el)
rfc_prec.write("\n")
for el1 in var_rec:
    rfc_recall.write("%f\t"%el1)
rfc_recall.write("\n")
for el2 in var_fsc:
    rfc_scores.write("%f\t"%el2)
rfc_scores.write("\n")
